package com.pl.labolatorium6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimesActivity extends AppCompatActivity {

    private EditText poland,newYork,london,tokyo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_times);
        poland =findViewById(R.id.plTime);
        newYork =findViewById(R.id.NYTime);
        london =findViewById(R.id.LondTime);
        tokyo =findViewById(R.id.TokTime);

        ScheduledExecutorService e= Executors.newSingleThreadScheduledExecutor();
        e.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                 LocalTime time = LocalTime.now();
                 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                 poland.setText(time.format(formatter));
                 time = LocalTime.now(ZoneId.of("Europe/London"));
                 london.setText(time.format(formatter));
                 time = LocalTime.now(ZoneId.of("America/New_York"));
                 newYork.setText(time.format(formatter));
                 time = LocalTime.now(ZoneId.of("Asia/Tokyo"));
                 tokyo.setText(time.format(formatter));
            }
        }, 0, 1, TimeUnit.SECONDS);

    }
}
