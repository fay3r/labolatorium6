package com.pl.labolatorium6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeActivity(View view) {
        Snackbar.make(view,"Pokazano czas",4000).setAction("Action",null).show();
        Intent intent = new Intent(MainActivity.this,TimesActivity.class);
        startActivity(intent);
    }
}
